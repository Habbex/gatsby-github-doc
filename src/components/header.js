import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { logout } from "../utils/auth";
import ThemeSwitcher from "../components/themeSwitcher";
import GitHubIcon from '@material-ui/icons/GitHub';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    textDecoration: 'none',
    padding: theme.spacing(1)
  },
}));


const Header = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <GitHubIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
           Reviso Git-docs
          </Typography>
          <ThemeSwitcher/>
          <IconButton edge="end" className={classes.menuButton} color="inherit"href="#logout" onClick={e => {
              logout()
              e.preventDefault()
            }} aria-label="logout">
              <Typography variant="h6" className={classes.title}>
              Logout
            </Typography>
            <ExitToAppIcon color="inherit" />
          </IconButton>   
        </Toolbar>
      </AppBar>
    </div>
  );
}


export default Header