# Git doc project

The project uses the following components:

## Gatsby.js
The web app is built with Gatsby JS , which is a React-based framework, GraqhQL powered static site generator. 

## Lunr.js 
The web app uses Lunr.js to index the pulled markdown files, in order to allow the user to search among the posts. Lunr.js is a bit like *Apache Solr* indexing solution.  Lunr.js is small , yet fully featured without the need for external server side services to handle the search. 

## Material-UI
The styling and components being used for the web app are provided by  Material-UI, which allow the developer to quickly build functioning web apps in no time. 

## Auth0 
The web app uses Auth0 for authentication and authorization of the user trying to access the web app. authentication and authorization platform. Basically, we make your login box awesome.

## GitLab 
The web app and article repositories are hosted on GitLab in order to utilize the built in CI/CD setup support, which allows the setup of automatic rebuilds and deployment to  hosted webserver when the master/ main branches are updated.

## Netilfy
Is a hosting provider which the GitLab CI/CD deploys to when the web app has been order to rebuild when either the web apps master branch has been updated or called by article repository when it's main branch has been updated. 

# How to run

In order to run the project you need to have a separate articles repository filled with markdown files, which then supply the git url for inside the gatsby-config.js file :

    {
    	resolve:  `gatsby-source-git`,
    
    	options: {
    
    	name:  `repo-one`,
    
    	remote:  `{git-url-to-article-repo}`,
    
    	branch:  `master`,
    
    	// Only import the docs folder from a codebase.
    
    	// patterns: [`src/**/*.{md, jpg, png}`]
    
    	patterns: [`src/**`]
    
    	},

You need to run `yarn build` first in order to pull the articles and cache them which Lunr.js will rely on to build the search index.

You can either disable the Auth0 login which handles the authentication and authorization located inside the components /index.js file :

    if (!isAuthenticated()) {
    
    login()
    
    return  <p>Redirecting to login...</p>
    
    }
Or you can create an Auth0 account and supply the required values inside a newly created .env file following this format bellow: 

    AUTH0_DOMAIN=domain URL
    AUTH0_CLIENTID=client id
    AUTH0_CALLBACK=callback URL
    
Finally you can run `yarn develop` , and view the web app by following the listed URL listed in the build logs which is usually http://localhost:8000. 