import React from "react"
import CustomThemeProvider from "./src/theme/CustomThemeProvider";
import CssBaseline from "@material-ui/core/CssBaseline";
import {silentAuth} from "./src/utils/auth"

class SessionCheck extends React.Component {
    constructor(props){
        super(props)
        this.state={
            loading:true
        }
    }

    handleCheckSession= ()=>{
        this.setState({loading: false})
    }

    componentDidMount(){
        silentAuth(this.handleCheckSession)
    }

    render(){
        return(
            this.state.loading === false && (
                <React.Fragment>{this.props.children}</React.Fragment>
            )
        )
    }
}

export const wrapRootElement=({element}) =>{
    console.info(`element`,element)
    // return <SessionCheck>  <CustomThemeProvider> {element} <CssBaseline/> </CustomThemeProvider></SessionCheck>
    return   <CustomThemeProvider> {element} <CssBaseline/> </CustomThemeProvider>
}